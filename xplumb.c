#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <X11/Xlib.h>

/* TODO:
 *  - working directoyies (very important!)
 *  - different plumb fields (attr, src, dest, type etc)
 *  - handle popen errors / programs not found
 *  - button2 interfering with out programs
 *  - some mechanism to check if we still have selected something?
*/

#define BUTTON      Button2
#define MODIFIER    AnyModifier
#define CLIPBOARD   "xclip -o "
#define PLUMB       "plumb "

int main(int argc, char* argv[])
{
	Display *dpy;
	XEvent  evt;
	FILE    *pipe;
	char    buff[BUFSIZ];
	char    command[BUFSIZ];

	if(!(dpy = XOpenDisplay(NULL))) {
		fprintf(stderr, "Couldn't open display\n");
		exit(1);
	}

	/* Use Button2 (middle mouse button) for plumbing */
	XGrabButton(dpy, BUTTON, MODIFIER, DefaultRootWindow(dpy), True, ButtonPressMask | ButtonReleaseMask, GrabModeAsync, GrabModeAsync, None, None);

	for(;;) {
		XNextEvent(dpy, &evt);
		if(evt.type == ButtonPress) {
			if(evt.xbutton.button == BUTTON) {
				pipe = popen(CLIPBOARD, "r");
				fgets(buff, sizeof(buff), pipe);

				if(strlen(buff) > 0) {
					strcpy(command, PLUMB);
					strcat(command, buff);
					pipe = popen(command, "r");
				}
			}
		}
	}

	pclose(pipe);
	XCloseDisplay(dpy);

	return 0;
}
